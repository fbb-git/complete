# complete

The complete C++ programmer

This project should eventually result in a book-like document 
about designing and implementing C++ programs.
The intention is to write a companion to the C++ Annotations
containing descriptions, hints, tips and exercises of C++ 
that go beyond the syntax description which is the main 
purpose of the Annotations. 
It's unknown When the project will reach its `version 1.00.00', 
if ever. Usable results might not be available for a year or so.
I might also abandon the project before that...
